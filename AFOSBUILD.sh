rm -rf /opt/ANDRAX/JS2PDFInjector

mvn package

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Create package... PASS!"
else
  # houston we have a problem
  exit 1
fi

mkdir /opt/ANDRAX/JS2PDFInjector

cp -Rf target/JS2PDFInjector.jar /opt/ANDRAX/JS2PDFInjector

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy JS2PDFInjector.jar... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
